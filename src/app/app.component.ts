import { Component, DoCheck, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { TokenStorageService } from './_services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  host: {'(window:scroll)': 'track($event)'},
})
export class AppComponent implements OnInit, DoCheck {
  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username?: string;
  isHome=false;
  pageYoffset:number=0
  searchForm:string=""
  province:string='Hà Nội'
  form: FormGroup = new FormGroup({
    province: new FormControl('Hà Nội'),
    utility:new FormArray([]),
  });
  constructor(private tokenStorageService: TokenStorageService,
    private route:ActivatedRoute) { }
  ngDoCheck(): void {
    this.isHome = window.location.pathname == "/home"
    this.ngOnInit()
  }

  ngOnInit(): void {
    this.isLoggedIn =  window.localStorage.getItem("is_authen") == "true"
   
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

      this.username = user.username;
    }
    
  }

  track($event : Event) {
    this.pageYoffset = window.pageYOffset
}
  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
    window.localStorage.setItem("is_authen", "false");
  }

  
  onCheckboxChange(e:any) {

    const checkArray: FormArray = this.form.get('utility') as FormArray;
  
    if (e.checked) {
      checkArray.push(new FormControl(e.source.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach(item => {
        if (item.value == e.source.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
  formatLabel(value: number) {
    if (value >= 1000000) {
      return Math.round(value / 1000000) + 'tr';
    } else if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }

    return value;
  }

}

