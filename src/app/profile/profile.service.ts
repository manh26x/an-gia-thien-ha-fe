import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../_services/token-storage.service';
const AUTH_API = 'http://localhost:8080/api/test/';
@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  

  
  constructor(private http: HttpClient,
    private tokenService:  TokenStorageService) { }


  
  getProfile(): Observable<any>  {
    let token:string  = "";
    token=this.tokenService.getToken()!
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' , 'Authorization' : token })};
    return this.http.get(AUTH_API + 'profile', httpOptions);
  }

  
  

}
