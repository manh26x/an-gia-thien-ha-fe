import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { News } from 'src/app/post-news/new';
import { ManagerNewsService } from '../manager-news.service';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.css']
})
export class NewsDetailComponent implements OnInit {

  news!: News 
  constructor(private newService: ManagerNewsService,
    private router: ActivatedRoute) { }

  ngOnInit(): void {
    this.newService.getNews(this.router.snapshot.paramMap.get('id')?.toString()).subscribe(res => {
      this.news = res
      
      for(let i=0; i< this.news.fileSource.length; i++) {
        this.news.fileSource[i] = "data:image/png;base64,"+this.news.fileSource[i];
      }
      console.log(this.news)
    })

  }

}
