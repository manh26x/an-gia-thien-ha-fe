import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/post-news/new';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { ManagerNewsService } from '../manager-news.service';

@Component({
  selector: 'app-your-news',
  templateUrl: './your-news.component.html',
  styleUrls: ['./your-news.component.css']
})
export class YourNewsComponent implements OnInit {

  newsArray!:Array<News>
  isAdmin = false
  status:number=0;
  constructor(private managerService: ManagerNewsService,
    private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isAdmin = this.tokenStorageService.getUser().roles.includes('ROLE_ADMIN')
    this.managerService.getMyNews(this.status).subscribe( res => {
      this.newsArray = res
      this.newsArray.forEach(news => {
        news.fileSource[0] = "data:image/png;base64,"+news.fileSource[0];
      })
    })
  }
  censored(id:number) {
    this.managerService.censored(id).subscribe( res => {
      this.ngOnInit();
    })
  }

}
