import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { News } from 'src/app/post-news/new';
import { ManagerNewsService } from '../manager-news.service';

@Component({
  selector: 'app-search-news',
  templateUrl: './search-news.component.html',
  styleUrls: ['./search-news.component.css']
})
export class SearchNewsComponent implements OnInit {
  newsArray!:Array<News>
  isOwner = false
  
  constructor(private newsService:ManagerNewsService,
    private router: ActivatedRoute) { }

  ngOnInit(): void {
    let search:string = ""
    if(this.router.snapshot.queryParamMap.get("search") !== null) {
      search=this.router.snapshot.queryParamMap.get("search")?.toString()!
      this.isOwner= true
    }
      
    this.newsService.searchNews(search).subscribe(res => {
      this.newsArray = res
      this.newsArray.forEach(news => {
        news.fileSource[0] = "data:image/png;base64,"+news.fileSource[0];
      })
    })
  }
}
