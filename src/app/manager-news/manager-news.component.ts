import { Component, OnInit } from '@angular/core';
import { News } from '../post-news/new';
import { TokenStorageService } from '../_services/token-storage.service';
import { ManagerNewsService } from './manager-news.service';

@Component({
  selector: 'app-manager-news',
  templateUrl: './manager-news.component.html',
  styleUrls: ['./manager-news.component.css']
})
export class ManagerNewsComponent implements OnInit {

  newsArray!:Array<News>
  isAdmin = false
  status:number=0
  constructor(private managerService: ManagerNewsService,
    private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isAdmin = this.tokenStorageService.getUser().roles.includes('ROLE_ADMIN')
    this.managerService.getUncensorNews(this.status).subscribe( res => {
      this.newsArray = res
      this.newsArray.forEach(news => {
        news.fileSource[0] = "data:image/png;base64,"+news.fileSource[0];
      })
    })
  }
  censored(id:number) {
    this.managerService.censored(id).subscribe( res => {
      this.ngOnInit();
    })
  }
}
