import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../_services/token-storage.service';

const AUTH_API = 'http://localhost:8080/api/news/';
@Injectable({
  providedIn: 'root'
})
export class ManagerNewsService {
  
  

  constructor(private http: HttpClient,
    private tokenService:  TokenStorageService) { }


    getUncensorNews(status:number):Observable<any> {
      let token:string  = "";
      token=this.tokenService.getToken()!
      let httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' , 'Authorization' : token })};
      return this.http.get(AUTH_API + 'uncensored/'+status, httpOptions);
    }
    getMyNews(status:number) :Observable<any> {
      let token:string  = "";
      token=this.tokenService.getToken()!
      let httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' , 'Authorization' : token })};
      return this.http.get(AUTH_API + "get/my-news/"+status, httpOptions);
    }
    getNews(id: any):Observable<any> {

      let httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })};
      return this.http.get(AUTH_API + "get/"+id, httpOptions);
    }
    censored(id:number) {
      let token:string  = "";
      token=this.tokenService.getToken()!
      let httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' , 'Authorization' : token })};
      return this.http.put(AUTH_API + "put/censored/"+id, httpOptions);
    }
    searchNews(keyword: string):Observable<any> {
      let httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json'})};
      return this.http.get(AUTH_API + "get/search?search="+keyword, httpOptions);
    }
}
