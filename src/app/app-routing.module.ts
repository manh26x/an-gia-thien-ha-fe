import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { PostNewsComponent } from './post-news/post-news.component';
import { AuthGuard } from './_services/auth.gaurd';
import { ManagerNewsComponent } from './manager-news/manager-news.component';
import { NewsDetailComponent } from './manager-news/news-detail/news-detail.component';
import { SearchNewsComponent } from './manager-news/search-news/search-news.component';
import { YourNewsComponent } from './manager-news/your-news/your-news.component';

const routes: Routes = [
  
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile',canActivate:[AuthGuard], component: ProfileComponent },
  { path: 'user',canActivate:[AuthGuard], component: BoardUserComponent },
  { path: 'mod',canActivate:[AuthGuard], component: BoardModeratorComponent },
  { path: 'admin',canActivate:[AuthGuard], component: BoardAdminComponent },
  { path: 'post-news',canActivate:[AuthGuard], component: PostNewsComponent },
  { path: 'news/:id', component: NewsDetailComponent },
  { path: 'search/news', component: SearchNewsComponent },
  { path: 'your-news',canActivate:[AuthGuard], component: YourNewsComponent },
  
  { path: 'manager-news',canActivate:[AuthGuard], component: ManagerNewsComponent },
  
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
