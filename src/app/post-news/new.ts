import { User } from "../_services/user"

export class News {
    public id!: number
	public city!: string
	public district!: string
	public ward!: string
	public street!: string
	public utility!: string
	public description!: string
	public title!: string
	public imgeUrl!: string
	public square!: number //m2
	public price!: number
	public capacity!: number
	public deposit!: number
	public status!: number
	public sex!: string
	public type!: string
	public numRoom!: number
    public number!: string
    public user:User = new User()
    public fileSource!:Array<any>
}