import { newArray } from '@angular/compiler/src/util';
import { Component, Inject, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { UploaderComponent, SelectedEventArgs, FileInfo, RemovingEventArgs } from '@syncfusion/ej2-angular-inputs';
import { createSpinner, showSpinner, hideSpinner } from '@syncfusion/ej2-popups';
import { EmitType, detach, Browser, createElement, isNullOrUndefined, EventHandler } from '@syncfusion/ej2-base';
import { DOCUMENT } from '@angular/common';
import { News } from './new';
import { NewsService } from './news.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { User } from '../_services/user';
import { Router } from '@angular/router';
@Component({
  selector: 'app-post-news',
  templateUrl: './post-news.component.html',
  styleUrls: ['./post-news.component.css']
})
export class PostNewsComponent implements OnInit {
  news : News = new News()
  images:Array<any> = [];
  newsForm: FormGroup = new FormGroup({
    type: new FormControl(),
    numRoom: new FormControl(),
    capacity: new FormControl(),
    sex: new FormControl(),
    square: new FormControl(),
    price: new FormControl(),
    deposit: new FormControl(),
  });
  addressForm: FormGroup = new FormGroup({
    city: new FormControl(),
    district: new FormControl(),
    ward: new FormControl(),
    street: new FormControl(),
    number: new FormControl(),
    utility: new FormArray([]),
    file: new FormControl(),
    fileSource: new FormControl('', [Validators.required])
  });
  contentForm: FormGroup =  new FormGroup({
    title: new FormControl(),
    description: new FormControl()
  });
  constructor(private _formBuilder: FormBuilder,
    private newsService: NewsService,
    private tokenService: TokenStorageService,
    private router:Router) {}

  ngOnInit() {

  }
  get f(){

    return "nothing";

  }
   

  remove(idx: number) : void {
    this.images.splice(idx,1)
  }

  onFileChange(event:any) {

    if (event.target.files && event.target.files[0]) {

        var filesAmount = event.target.files.length;

        for (let i = 0; i < filesAmount; i++) {

                var reader = new FileReader();

   

                reader.onload = (event:any) => {

                   this.images.push(event.target.result); 
                   console.log(event.target.result);

                   this.addressForm.patchValue({
                    fileSource: this.images
                  })
                }
                
                reader.readAsDataURL(event.target.files[i]);

        }

    }

}
onCheckboxChange(e:any) {

  const checkArray: FormArray = this.addressForm.get('utility') as FormArray;

  if (e.checked) {
    checkArray.push(new FormControl(e.source.value));
  } else {
    let i: number = 0;
    checkArray.controls.forEach(item => {
      if (item.value == e.source.value) {
        checkArray.removeAt(i);
        return;
      }
      i++;
    });
  }
}
showForm(){
  debugger
  console.log(this.newsForm.value)
  console.log(this.addressForm.value)
}
  save() {
   this.news.city = this.addressForm.value.city
   this.news.district = this.addressForm.value.district
   this.news.ward = this.addressForm.value.ward
   this.news.street =this.addressForm.value.street
   this.news.number = this.addressForm.value.number
   this.news.utility=this.addressForm.value.utility.toString()
    this.news.fileSource = this.addressForm.value.fileSource

   this.news.type=this.newsForm.value.type
   this.news.capacity=this.newsForm.value.capacity
   this.news.deposit=this.newsForm.value.deposit
   this.news.numRoom=this.newsForm.value.numRoom
   this.news.square=this.newsForm.value.square
   this.news.sex=this.newsForm.value.sex
   this.news.price=this.newsForm.value.price
   this.news.title=this.contentForm.value.title
   this.news.description=this.contentForm.value.description
   let user:User = this.tokenService.getUser()
   this.news.user = user
   this.newsService.postNews(this.news).subscribe(awnser => {
    this.router.navigateByUrl("/home");
   })
  }
}
