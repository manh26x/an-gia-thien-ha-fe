import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../_services/token-storage.service';
import { News } from './new';
const AUTH_API = 'http://localhost:8080/api/news/';
@Injectable({
  providedIn: 'root'
})
export class NewsService {
  
 
  constructor(private http: HttpClient,
    private tokenService:  TokenStorageService) { }
   
  public postNews(news : News)  : Observable<any> {
    let token:string  = "";
    token=this.tokenService.getToken()!
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' , 'Authorization' : token })};
    return this.http.post(AUTH_API + "post", JSON.stringify(news), httpOptions);
  }
}
